#Getting started

After completing the installation you can start using the app. On first access the application will download the data set, from which it will get the information to provide you with the response about a substance, and from this moment on, you can use the app even without Internet connection.
To check a substance then, write the name in the **searching field** and tap *Search* (note that when you start typing, the app suggests the term).
The section below the searching field , **Recent Audits**, will show the outcome of the analysis, which can be of 4 types:

* "***Approved***": the EU allows the use of the substance searched in food and / or cosmetics
* "***Prohibited***": The EU prohibits the use of the substance searched in food and / or cosmetics
* "***Unsure***": the EU does not provide reliable information about the substance
* "***Not Found***": the searched substance is not present in the dataset

The date appearing by the outcome is about the last European council that determined the  status.

!!!Tip
    You can get the result about several items at once by writing in the searching field the names of the substances separated by a comma.

!!!Warning
    It is essential the device you installed the app upon is ( Internet connetcted) when you first use the application, in order to get the dataset downloaded. Only from this point on, the Internet connection will not be strictly necessary. However, if the device is not connected for long periods, it will increase the risk the app will produce incorrect results, not being able to download the updated version of the dataset.
