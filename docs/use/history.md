#History
**Recent Audits**  section lists all substances searched by the user and their outcome. This way the user can always see the results of analyzes carried out previously and repeat them to check for any changes as a result of data set updates.
