#Dataset Update

If the device on which the app has been installed is often or always connected to the Internet, the dataset will be updated regularly and the user will be informed of that by notification on his own device. Moreover, if on **History** section there are scansions that have produced an "Unsure" or "***Not Found***" type of outcome, they will be automatically repeated after this upgrade. The user will be notified of the scans carried out (and the possible change of outcome) through notification of successful update.
