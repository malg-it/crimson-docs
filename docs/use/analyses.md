#Analyses of results

The first data about the substance to be checked are immediately presented in Recent Audits ,at the time of search as described in [Getting Started](start.md). However if you need more information on the result achieved, on "**Recent Audits**" tap the item containing the name of the substance concerned. You will see an explanation page about the above substance, in which will be specified some details including the category ingredient belongs to, the date of acquisition of the same, and the date of the last confirmation by the EU on the state of the substance. If the substance was placed in a sequence of items on the searching field, you can see the other items by selecting "*Test Related*". 


!!!Warning
    If the device has been offline for a long period, the data may not be updated.

!!!Tip 
    In this page is set up a direct link to Wikipedia. Tap the respective option to see additional data relating the substance concerned.

