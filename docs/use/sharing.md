#Sharing and communication

To share on a social network the result obtained from analysis, tap the "Share" option on the menu, and choose the desired platform.
To contact the Malg group, tap "Contact us".