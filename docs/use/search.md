#Searching by List

There is an alternative way of research, especially useful when the name of the substance to be analyzed is uncertain. You can find your desired ingredient directly from the list of names of the substances present in the app.To consult the list, tap *"Substances index"*, in the *Menu*, then select the substance of interest. Each term is already marked by one of the three states "***Approved***", "***Prohibited***" or "***Unsure***" described in the previous section.
