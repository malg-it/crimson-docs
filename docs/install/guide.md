#Viewing user guide and manual
To view a guide about the main functions of the app, access the *Menu*, then tap *Guide*. By the final part of the guide you can instead refer to the complete manual (that would be this document).
