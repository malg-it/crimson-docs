# Crimson Project

The purpose of this app is to provide information about the safety of certain additive substances present in food and cosmetic products, especially those acquired or from extra-EU countries. These indications are obtained using **Open Data**, collected in **datasets**, published by the European Union and concerning the approval or prohibition of use of a substance in a food or cosmetic product.

You can download the app (in [APK format](https://bitbucket.org/malg-it/crimson-android/downloads/it.malg.crimson.apk)) straight from the official repository (where [source-code](https://bitbucket.org/malg-it/crimson-android) is also hosted).

!!!note "License"
    The app is to be distributed according to the terms of the [GPL3 license](license.md).

!!!Warning 
    The application is intended solely for purposes related to the user's preferences about the products to be utilized or to be purchased, and not for relief, treatment or prevention of diseases.
    Any information obtained /consulted through this application will only be provided to help the user and should not in any way be considered and interpreted as medical advice.
    All information obtained through the use of this application may not be adequate, accurate, complete, reliable.
