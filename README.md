#_Crimson_ Manual 

ReadTheDocs status: [![Documentation Status](https://readthedocs.org/projects/crimson/badge/?version=latest)](http://crimson.readthedocs.io/en/latest/?badge=latest)

English manual for the _Crimson_ project.